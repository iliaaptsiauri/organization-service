const expect = require('chai').expect;
const helper = require('../../../../app/services/helpers/organization');
const data = require('../../../fixtures/data');

describe('Organization service helper', () => {
  describe('#depthFirstTree', () => {
    it('should return depth first tree', () => {
      const depthFirstTree = helper.depthFirstTree(data.request);
      expect(depthFirstTree).to.deep.eql(data.depthFirstTree);
    });
  });
});
