const expect = require('chai').expect;
const sinon = require('sinon');
const helper = require('../../../app/presenters/helpers/organization');
const presenter = require('../../../app/presenters/organization');

describe('Organization presenter', () => {
  describe('#paginate', () => {
    it('should paginate the array', () => {
      const inital = [1, 2, 3, 4];
      const expected = [1, 2];

      const paginated = presenter.paginate(inital, 1, 2);
      expect(paginated).to.eql(expected);
    });
  });

  describe('#relateAndSortOrganizations', () => {
    let initial;
    beforeEach((done) => {
      initial = {
        parents: [],
        sisters: [{ orgName: 'C' }, { orgName: 'b' }],
        daughters: [{ orgName: 'A' }]
      };
      done();
    });

    it('should call sortByName once', () => {
      const sortByName = sinon.spy(helper, 'sortByName');

      presenter.relateAndSortOrganizations(initial);
      sortByName.restore();
      expect(sortByName.calledOnce).to.be.ok;
    });

    it('should relate and return sorted organizations', () => {
      const expected = [
        { relationship_type: 'daughters', org_name: 'A' },
        { relationship_type: 'sister', org_name: 'b' },
        { relationship_type: 'sister', org_name: 'C' }
      ];
      const related = presenter.relateAndSortOrganizations(initial);
      expect(related).to.deep.eql(expected);
    });
  });
});
