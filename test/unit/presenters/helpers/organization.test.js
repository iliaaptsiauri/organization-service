const expect = require('chai').expect;
const helper = require('../../../../app/presenters/helpers/organization');

describe('Organization presenter helper', () => {
  it('should sort organization by org_name', () => {
    const inital = [{ org_name: 'f' }, { org_name: 'Z' }, { org_name: 'A' }];
    const expected = [{ org_name: 'A' }, { org_name: 'f' }, { org_name: 'Z' }];

    const sorted = helper.sortByName(inital);
    expect(sorted).to.have.ordered.deep.members(expected);
  });
});
