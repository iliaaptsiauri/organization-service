const fixtures = exports;

fixtures.request = {
  org_name: 'Paradise Island',
  daughters: [{
    org_name: 'Banana tree',
    daughters: [{
      org_name: 'Yellow Banana'
    }, {
      org_name: 'Brown Banana'
    }, {
      org_name: 'Black Banana',
      daughters: [{
        org_name: 'Test Banana'
      },
      {
        org_name: 'Wow Banana'
      }
      ]
    }]
  }]
};

fixtures.depthFirstTree = [
  { org_name: 'Yellow Banana', daughters: [] },
  { org_name: 'Brown Banana', daughters: [] },
  { org_name: 'Test Banana', daughters: [] },
  { org_name: 'Wow Banana', daughters: [] },
  { org_name: 'Black Banana', daughters: [{ org_name: 'Test Banana', daughters: [] }, { org_name: 'Wow Banana', daughters: [] }] },
  { org_name: 'Banana tree', daughters: [{ org_name: 'Yellow Banana', daughters: [] }, { org_name: 'Brown Banana', daughters: [] }, { org_name: 'Black Banana', daughters: [{ org_name: 'Test Banana', daughters: [] }, { org_name: 'Wow Banana', daughters: [] }] }] },
  { org_name: 'Paradise Island', daughters: [{ org_name: 'Banana tree', daughters: [{ org_name: 'Yellow Banana', daughters: [] }, { org_name: 'Brown Banana', daughters: [] }, { org_name: 'Black Banana', daughters: [{ org_name: 'Test Banana', daughters: [] }, { org_name: 'Wow Banana', daughters: [] }] }] }] }
];


fixtures.response = [{ relationship_type: 'daughters', org_name: 'Black Banana' },
  { relationship_type: 'daughters', org_name: 'Brown Banana' },
  { relationship_type: 'parent', org_name: 'Paradise Island' },
  { relationship_type: 'daughters', org_name: 'Yellow Banana' }];
