const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const db = require('../../app/models');
const data = require('../fixtures/data');

describe('Organziations endpoint', () => {
  before((done) => {
    db.sequelize.sync({ force: true }).then(() => done());
  });

  describe('POST /organizations', () => {
    it('should create orgainzations and relate them', (done) => {
      request(app)
        .post('/organizations')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send(JSON.stringify(data.request))
        .expect(204)
        .end((err, _) => {
          if (err) {
            return done(err);
          }

          return done();
        });
    });
  });

  describe('GET /organizations', () => {
    it('should return related organizations', (done) => {
      request(app)
        .get('/organizations/Banana tree')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          expect(res.body).to.deep.equal(data.response);
          return done();
        });
    });
  });
});
