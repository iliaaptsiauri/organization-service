const env = process.env.NODE_ENV || 'development';
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../../config/database.js')[env];

const basename = path.basename(module.filename);
const db = {};
const sequelize = new Sequelize(config.database, config.username, config.password, config);

fs
  .readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// Associations
db.Organization.belongsToMany(db.Organization, {
  as: 'Parents',
  through: 'OrganizationParent',
  foreignKey: 'daughter_id'
});

db.Organization.belongsToMany(db.Organization, {
  as: 'Daughters',
  through: 'OrganizationParent',
  foreignKey: 'parent_id'
});

if (env === 'development') {
  db.sequelize.sync();
}

module.exports = db;
