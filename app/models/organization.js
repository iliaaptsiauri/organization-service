module.exports = (sequelize, DataTypes) => {
  const schema = {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    orgName: { type: DataTypes.STRING, field: 'org_name', allowNull: false },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at', allowNull: false, defaultValue: DataTypes.NOW },
    createdAt: { type: DataTypes.DATE, field: 'created_at', allowNull: false, defaultValue: DataTypes.NOW }
  };

  const indexes = [{ fields: ['org_name'] }];

  const Organization = sequelize.define('Organization', schema, { indexes });

  Organization.getSisters = (orgName, parentIds) => (
    Organization.findAll({
      where: {
        orgName: {
          $ne: orgName
        }
      },
      include: [
        { association: 'Parents', where: { id: parentIds } }
      ] })
    );

  return Organization;
};
