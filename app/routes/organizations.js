const express = require('express');
const organizationService = require('../services/organization');
const organizationPresenter = require('../presenters/organization');

const router = express.Router();

router.post('/', (req, res, next) => {
  const body = req.body;
  organizationService.saveOrganizationTree(body)
  .then(() => {
    res.sendStatus(204);
  }).catch((e) => {
    next(e);
  });
});

router.get('/:org_name', (req, res, next) => {
  const orgName = req.params.org_name;
  const page = req.query.page || 1;
  const limit = req.query.limit || 100;

  organizationService.getRelated(orgName)
  .then((related) => {
    const sortedRelation = organizationPresenter.relateAndSortOrganizations(related);
    const response = organizationPresenter.paginate(sortedRelation, page, limit);
    res.json(response);
  }).catch((e) => {
    next(e);
  });
});

module.exports = router;
