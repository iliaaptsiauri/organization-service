const organizationHelper = require('./helpers/organization');

const presenter = exports;

function responseObject(type, orgs) {
  return orgs.map(org => ({ relationship_type: type, org_name: org.orgName }));
}

presenter.relateAndSortOrganizations = (related) => {
  const parents = responseObject('parent', related.parents);
  const sisters = responseObject('sister', related.sisters);
  const daughters = responseObject('daughters', related.daughters);
  return organizationHelper.sortByName(parents.concat(sisters, daughters));
};

presenter.paginate = (arr, page, limit) => {
  const begin = (page - 1) * limit;
  const end = page * limit;

  return arr.slice(begin, end);
};
