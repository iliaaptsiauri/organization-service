exports.sortByName = orgs => (
  orgs.sort((a, b) => a.org_name.localeCompare(b.org_name))
);
