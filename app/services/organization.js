const db = require('../models');
const helper = require('./helpers/organization');

const service = exports;

function saveDepthFirstTree(orgResource) {
  const daughterNames = orgResource.daughters.map(o => o.org_name);
  return db.Organization.findOne({
    where: {
      orgName: orgResource.org_name
    }
  }).then(org => (
    org || db.Organization.create({ orgName: orgResource.org_name })
  )).then(entity => ([
    entity,
    db.Organization.findAll({
      where: {
        orgName: daughterNames
      }
    })]
  )).spread((entity, daughters) => entity.addDaughters(daughters))
  .catch((e) => {
    throw new Error(e);
  });
}

service.getRelated = orgName => (
  db.Organization.findOne({
    where: { orgName }
  })
  .then((entity) => {
    if (!entity) {
      throw new Error('Not found');
    }
    return [
      entity.getParents(),
      entity.getDaughters()
    ];
  }).spread((parents, daughters) => {
    const parentIds = parents.map(el => el.id);
    return [
      parents,
      daughters,
      db.Organization.getSisters(orgName, parentIds)
    ];
  }).spread((parents, daughters, sisters) => ({
    parents,
    daughters,
    sisters
  }))
  .catch((e) => {
    throw new Error(e);
  })
);

service.saveOrganizationTree = (data) => {
  const dFTree = helper.depthFirstTree(data);
  const savePromises = dFTree.map(saveDepthFirstTree);
  return Promise.all(savePromises);
};
