exports.depthFirstTree = (rootOrg) => {
  const orgArray = [];
  (function recurse(current) {
    current.daughters = current.daughters || [];
    current.daughters.forEach((daughter) => {
      recurse(daughter, current);
    });
    orgArray.push(current);
  }(rootOrg));

  return orgArray;
};
