module.exports = {
  development: {
    dialect: 'sqlite',
    storage: 'databases/dev_database.sqlite'
  },
  test: {
    dialect: 'sqlite',
    storage: 'databases/test_database.sqlite',
    logging: false
  }
};
